#!/usr/bin/env python3
import sys
import re

filename_gff3 = 'xenTro9_XB2021-04-16.ucsc.gff3'
filename_id_list = 'xenTro9_XB2021-04-16.ucsc.id_list.txt'

gene2tx = dict()
tx2gene = dict()
exon2gene = dict()
cds2gene = dict()
f_list = open(filename_id_list, 'r')
for line in f_list:
    tokens = line.strip().split("\t")
    gene_id = tokens[1]
    tx_id = tokens[2]
    if gene_id not in gene2tx:
        gene2tx[gene_id] = []
    gene2tx[gene_id].append(tx_id)
    tx2gene[tx_id] = gene_id

    if tokens[0] == 'Exon':
        exon2gene[tokens[3]] = {'gene': gene_id, 'tx': tx_id}
    elif tokens[0] == 'CDS':
        cds2gene[tokens[3]] = {'gene': gene_id, 'tx': tx_id}
f_list.close()


filename_base = re.sub(r'\.gff(.)*$', '', filename_gff3)

f_out = dict()
for i in range(1,4):
    f_out[i] = open('%s.L%d.gff3' % (filename_base, i), 'w')
    f_out[i].write('##gff-version 3\n')

sys.stderr.write('Process %s...\n' % filename_gff3)

gene_list = dict()
tx_list = dict()
tx2exon = dict()
tx2cds = dict()
ignore_gene_count = 1

f_in = open(filename_gff3, 'r')
for line in f_in:
    if line.startswith('#'):
        continue

    tokens = line.strip().split("\t")
    tmp_coord = '\t'.join(tokens[:8])
    seq_id = tokens[0]
    mol_type = tokens[2]
    tmp_attr = tokens[-1]

    # manual fix for a typo
    if tmp_attr.startswith('ID=gene11611;'):
        tmp_attr = tmp_attr.replace(';Xenbase:XB-GENE-22062234', '')
    
    # ref. HUMAN GENCODE r37
    # gene: ID, gene_id, gene_type, gene_name, level
    # transcript: ID, Parent, gene_id, gene_type, gene_name, transcript_id, transcript_type, transcript_name, level
    # exon: ID, Parent(tx), transcript_id, gene_id, gene_type, gene_name, transcript_type, transcript_name, transcript_support_level, exon_number, exon_id 
    # CDS: ID, Parent(tx), transcript_id, gene_id, transcript_id, gene_type, gene_name, transcript_type, transcript_name, transcript_support_level, exon_number, exon_id, protein_id

    # chr19	HAVANA	gene	5993164	6199572	.	-	.	ID=ENSG00000087903.13;gene_id=ENSG00000087903.13;gene_type=protein_coding;gene_name=RFX2;level=1;hgnc_id=HGNC:9983;tag=overlapping_locus;havana_gene=OTTHUMG00000180711.5
    # chr19	HAVANA	transcript	5993164	6110500	.	-	.	ID=ENST00000303657.10;Parent=ENSG00000087903.13;gene_id=ENSG00000087903.13;transcript_id=ENST00000303657.10;gene_type=protein_coding;gene_name=RFX2;transcript_type=protein_coding;transcript_name=RFX2-201;level=2;protein_id=ENSP00000306335.4;transcript_support_level=1;hgnc_id=HGNC:9983;tag=basic,MANE_Select,appris_principal_2,CCDS;ccdsid=CCDS12157.1;havana_gene=OTTHUMG00000180711.5;havana_transcript=OTTHUMT00000452687.2
    # chr19	HAVANA	exon	6110393	6110500	.	-	.	ID=exon:ENST00000303657.10:1;Parent=ENST00000303657.10;gene_id=ENSG00000087903.13;transcript_id=ENST00000303657.10;gene_type=protein_coding;gene_name=RFX2;transcript_type=protein_coding;transcript_name=RFX2-201;exon_number=1;exon_id=ENSE00002893133.2;level=2;protein_id=ENSP00000306335.4;transcript_support_level=1;hgnc_id=HGNC:9983;tag=basic,MANE_Select,appris_principal_2,CCDS;ccdsid=CCDS12157.1;havana_gene=OTTHUMG00000180711.5;havana_transcript=OTTHUMT00000452687.2
    # chr19	HAVANA	exon	6047407	6047504	.	-	.	ID=exon:ENST00000303657.10:2;Parent=ENST00000303657.10;gene_id=ENSG00000087903.13;transcript_id=ENST00000303657.10;gene_type=protein_coding;gene_name=RFX2;transcript_type=protein_coding;transcript_name=RFX2-201;exon_number=2;exon_id=ENSE00003568552.1;level=2;protein_id=ENSP00000306335.4;transcript_support_level=1;hgnc_id=HGNC:9983;tag=basic,MANE_Select,appris_principal_2,CCDS;ccdsid=CCDS12157.1;havana_gene=OTTHUMG00000180711.5;havana_transcript=OTTHUMT00000452687.2
    # chr19	HAVANA	CDS	6047407	6047496	.	-	0	ID=CDS:ENST00000303657.10;Parent=ENST00000303657.10;gene_id=ENSG00000087903.13;transcript_id=ENST00000303657.10;gene_type=protein_coding;gene_name=RFX2;transcript_type=protein_coding;transcript_name=RFX2-201;exon_number=2;exon_id=ENSE00003568552.1;level=2;protein_id=ENSP00000306335.4;transcript_support_level=1;hgnc_id=HGNC:9983;tag=basic,MANE_Select,appris_principal_2,CCDS;ccdsid=CCDS12157.1;havana_gene=OTTHUMG00000180711.5;havana_transcript=OTTHUMT00000452687.2

    tmp_id = 'NA'
    tmp_parent = 'NA'
    tmp_name = 'NotAvail'
    tmp_biotype = 'NA'

    for tmp in tmp_attr.split(';'):
        if tmp == '':
            continue

        try:
            (tmp_k, tmp_v) = tmp.split('=')

        except Exception:
            sys.stdout.write("Attr Error: %s" % tmp_attr)

        if tmp_k == 'ID':
            tmp_id = tmp_v
        if tmp_k == 'Parent':
            tmp_parent = tmp_v
        if tmp_k == 'Name':
            tmp_name = tmp_v.lower()
        if tmp_k == 'gene_biotype':
            tmp_biotype= tmp_v
    
    if tmp_id == 'NA':
        sys.stderr.write('No ID: %s\n' % tmp_id)
        sys.exit(1)
    
    if mol_type in ['gene', 'tRNA', 'rRNA']:
        # bad record to ignore
        if tmp_id not in gene2tx:
            sys.stderr.write('Ignore %s (%d)\n' % (tmp_id, ignore_gene_count))
            ignore_gene_count += 1
            continue

        tmp_level = 1

        # Determine the level with gene name
        if tmp_name.find('provisional:') >= 0:
            tmp_name = tmp_name.split('provisional:')[1].replace(']', '')
            tmp_level = 2
        if tmp_name.find('-like') >= 0:
            tmp_level = 2

        if tmp_name.upper().startswith('LOC'):
            tmp_name = tmp_name.upper()
            tmp_level = 3
        if tmp_name.upper().startswith('XETROV'):
            tmp_name = tmp_name.capitalize()
            tmp_level = 3

        if tmp_name.startswith('XB') and tmp_name.find('[provisional]') >= 0:
            tmp_name = tmp_name.split()[0]
            tmp_level = 3
        if tmp_name.upper().startswith('GM') and len(tmp_name) >= 6:
            tmp_name = tmp_name.upper()
            tmp_level = 3
        if tmp_name.upper().startswith('RGD') and len(tmp_name) >= 6:
            tmp_name = tmp_name.upper()
            tmp_level = 3
        if tmp_name.upper().startswith('MGC') and len(tmp_name) >= 6:
            tmp_name = tmp_name.upper()
            tmp_level = 3
        if tmp_name.upper().startswith('KIAA') and len(tmp_name) >= 6:
            tmp_level = 3
        
        # Manual fix for dubious gene name
        if tmp_name == 'fucolectinloc100491211':
            tmp_name = 'fucolectin'
            tmp_level = 2
        if tmp_name == 'gnrhr2/nmi':
            tmp_name = 'gnrhr2_nmi'
            tmp_level = 2
        if tmp_name == 'homeobox100496651-provisional':
            tmp_name = 'homeobox100496651'
            tmp_level = 3
        if tmp_name == 'linc02210-crhr1':
            tmp_level = 3
        if tmp_name == 'linc02867':
            tmp_level = 3
        if tmp_name == 'mhc100494167':
            tmp_name = tmp_name.upper()
            tmp_level = 3
        if tmp_name.startswith('XB5802730'):
            tmp_name = 'rab101'
            tmp_level = 2
        if tmp_name.startswith('XB940515'):
            tmp_name = 'tnfsf8'
            tmp_level = 2
        
        # manual fix for biotype
        if tmp_name.startswith('mir'):
            tmp_biotype = 'microRNA'
        
        # new AmphiBase gene ID
        ab_gene_id = 'ABXETG%07d.1' % ( int(tmp_id.replace('gene', '')) )

        tmp_gene_str = 'gene_id=%s;gene_type=%s;gene_name=%s;level=%d;' % \
                        (ab_gene_id, tmp_biotype, tmp_name, tmp_level)
        
        gene_list[ab_gene_id] = {'str': tmp_gene_str, 'type': tmp_biotype, 'level': tmp_level}
        f_out[tmp_level].write('%s\tID=%s;%s\n' % (tmp_coord, ab_gene_id, tmp_gene_str))

    elif mol_type in ['mRNA', 'lnc_RNA', 'primary_transcript', 'transcript', 'miRNA']:
        # bad record to ignore
        if tmp_id not in tx2gene:
            sys.stderr.write('Ignore %s tx (%d)\n' % (tmp_id, ignore_gene_count))
            ignore_gene_count += 1
            continue

        ab_tx_id = 'ABXETT%07d.1' % (int(tmp_id.replace('rna', '')))
        ab_gene_id = 'ABXETG%07d.1' % (int(tmp_parent.replace('gene', '')))

        tmp_tx_str = 'transcript_id=%s;transcript_type=%s;transcript_support_level=2;' % (ab_tx_id, gene_list[ab_gene_id]['type'])
        tmp_tx_str += gene_list[ab_gene_id]['str']
        tx_list[ab_tx_id] = tmp_tx_str

        tmp_level = gene_list[ab_gene_id]['level']
        f_out[tmp_level].write('%s\tID=%s;Parent=%s;%s\n' % (tmp_coord, ab_tx_id, ab_gene_id, tmp_tx_str))

    elif mol_type == 'exon':
        # manual fix 
        if tmp_parent == 'gene2763':
            tmp_parent = 'rna9130'

        if tmp_parent.startswith('gene'):
            print(line.strip())
            continue
        
        if tmp_id not in exon2gene:
            sys.stderr.write("Ignore exon: %s\n" % tmp_id)
            continue
        
        tmp_tx_id = exon2gene[tmp_id]['tx']
        ab_tx_id = 'ABXETT%07d.1' % (int(tmp_tx_id.replace('rna', '')))

        if ab_tx_id not in tx2exon:
            tx2exon[ab_tx_id] = 1
        else:
            tx2exon[ab_tx_id] += 1

        exon_number = tx2exon[ab_tx_id]
        exon_id = 'exon:%s:%d' % (ab_tx_id, exon_number)

        # exon: ID, Parent(tx), transcript_id, gene_id, gene_type, gene_name, transcript_type, transcript_name, transcript_support_level, exon_number, exon_id 
        tmp_exon_str = tx_list[ab_tx_id]
        tmp_exon_str += 'exon_number=%d;' % exon_number

        f_out[tmp_level].write('%s\tID=%s;Parent=%s;%s\n' % (tmp_coord, exon_id, ab_tx_id, tmp_exon_str))
    elif mol_type == 'CDS':
        # CDS: ID, Parent(tx), transcript_id, gene_id, transcript_id, gene_type, gene_name, transcript_type, transcript_name, transcript_support_level, exon_number, exon_id, protein_id

        if tmp_parent.startswith('gene'):
            print(line.strip())
            continue
        
        if tmp_id not in cds2gene:
            sys.stderr.write("Ignore cds: %s\n" % tmp_id)
            continue
        
        tmp_tx_id = cds2gene[tmp_id]['tx']
        ab_tx_id = 'ABXETT%07d.1' % (int(tmp_tx_id.replace('rna', '')))

        if ab_tx_id not in tx2cds:
            tx2cds[ab_tx_id] = 1
        else:
            tx2cds[ab_tx_id] += 1

        tmp_exon_str = tx_list[ab_tx_id]
        # tmp_exon_str += 'exon_number=%d;' % exon_number

        f_out[tmp_level].write('%s\tID=%s;Parent=%s;%s\n' % (tmp_coord, exon_id, ab_tx_id, tmp_exon_str))
 
f_in.close()

for i in range(1,4):
    f_out[i].close()
