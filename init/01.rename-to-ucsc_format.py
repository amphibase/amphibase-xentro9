#!/usr/bin/env python3
import gzip
import sys
import re

filename_gff3 = 'xenTro9_XB2021-04-16.gff3.gz'
filename_report = 'GCF_000004195.3_Xenopus_tropicalis_v9.1_assembly_report.txt.gz'

filename_base = re.sub(r'\.gff(.)*$', '', filename_gff3)

filename_out = '%s.ucsc.gff3' % filename_base
f_out = open(filename_out, 'w')

orig2ucsc = dict()
f_report = gzip.open(filename_report, 'rt')
for line in f_report:
    if line.startswith('#'):
        continue
    tokens = line.strip().split("\t")
    orig_id = tokens[0]
    ucsc_id = tokens[-1]
    orig2ucsc[orig_id] = ucsc_id
f_report.close()

# Sequence-Name	Sequence-Role	Assigned-Molecule	Assigned-Molecule-Location/Type	GenBank-Accn	Relationship	RefSeq-Accn	Assembly-Unit	Sequence-Length	UCSC-style-name
# Chr01	assembled-molecule	1	Chromosome	CM004443.1	=	NC_030677.1	Primary Assembly	194866763	chr1
# unplaced_34     unplaced-scaffold       na      na      KV467167.1      =       NW_016690179.1  Primary Assembly        1699    chrUn_NW_016690179v1
# MT      assembled-molecule      MT      Mitochondrion   na      <>      NC_006839.1     non-nuclear     17610   chrM

sys.stderr.write('Process %s...\n' % filename_gff3)
f_in = gzip.open(filename_gff3, 'rt')

for line in f_in:
    if line.startswith('#'):
        f_out.write('%s\n' % line.strip())
        continue

    tokens = line.strip().split("\t")
    seq_id = tokens[0]
    f_out.write('%s\t%s\n' % (orig2ucsc[seq_id], '\t'.join(tokens[1:])))
f_in.close()
f_out.close()
