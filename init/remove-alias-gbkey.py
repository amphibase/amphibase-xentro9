#!/usr/bin/env python3
import sys
import os

filename_gff3 = 'xenTro9_XB2021-04-16.ucsc.gff3'
filename_out = filename_gff3.replace('.gff3', '') + '+NoAlias.gff3'

sys.stderr.write('Process %s...\n' % filename_gff3)


f_out = open(filename_out, 'w')
f_gff = open(filename_gff3, 'r')
for line in f_gff:
    if line.startswith('#'):
        f_out.write("%s\n" % line.strip())
        continue

    tokens = line.strip().split("\t")

    attr_list = dict()
    for tmp in tokens[8].split(';'):
        if tmp == '':
            continue
        try:
            (tmp_k, tmp_v) = tmp.split('=')
            attr_list[tmp_k] = tmp_v

        except Exception:
            sys.stderr.write('KeyError: %s\n' % line.strip())
            f_out.write("%s\n" % line.strip())
    
    if 'Alias' in attr_list:
        if attr_list['ID'] != attr_list['Alias'].replace('XT-9_1', ''):
            print(attr_list['ID'], attr_list['Alias'])
f_gff.close()
f_out.close()
