#!/usr/bin/env python3
import sys
import re

filename_gff3 = sys.argv[1]

filename_base = re.sub(r'\.gff(.)*$', '', filename_gff3)

filename_out = '%s.ucsc.gff3' % filename_base
f_out = open(filename_out, 'w')

orig2ucsc = dict()
f_tsv = open('./data/xenTro9.id.tsv', 'r')
for line in f_tsv:
    tokens = line.strip().split("\t")
    orig_id = tokens[1]
    ucsc_id = tokens[0]
    orig2ucsc[orig_id] = ucsc_id
f_tsv.close()

sys.stderr.write('Process %s...\n' % filename_gff3)
f_in = open(filename_gff3, 'r')
for line in f_in:
    if line.startswith('#'):
        f_out.write('%s\n' % line.strip())
        continue

    tokens = line.strip().split("\t")
    seq_id = tokens[0]
    f_out.write('%s\t%s\n' % (orig2ucsc[seq_id], '\t'.join(tokens[1:])))
f_in.close()
f_out.close()
