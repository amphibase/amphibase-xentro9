#!/usr/bin/env python3
import os
import sys
import gzip

usage_mesg = 'Usgae: %s <gff3> <dirname_out>' % (sys.argv[0])

if len(sys.argv) != 3:
    sys.stderr.write('\n%s\n\n' % usage_mesg)
    sys.exit(1)

filename_gff3 = sys.argv[1]
dirname_out = sys.argv[2]

f_gff3 = open(filename_gff3, 'r')
if filename_gff3.endswith('.gz'):
    f_gff3 = gzip.open(filename_gff3, 'rt')

headers = []
f_out_list = dict()
for line in f_gff3:
    if line.startswith('#'):
        headers.append(line.strip())
        continue

    tokens = line.strip().split("\t")
    seq_id = tokens[0]
    seq_type = tokens[2]

    if seq_id == 'MT':
        seq_id = 'chrM'
    elif seq_id.startswith('Chr'):
        seq_id = seq_id.replace('Chr', 'chr')
    elif not seq_id.startswith('chr'):
        seq_id = 'chrUn'

    if seq_id not in f_out_list:
        sys.stderr.write('seq_id: %s\n' % seq_id)
        filename_out = os.path.join(dirname_out, '%s.all.gff3' % seq_id)
        f_out_list[seq_id] = open(filename_out, 'w')
        f_out_list[seq_id].write('#gff-version 3\n')

    f_out_list[seq_id].write(line.strip()+"\n")
f_gff3.close()
